# Авторы | Authors

Calmira GNU/Linux была собрана | Calmira GNU/Linux was build by

Над дистрибутивом работали:
* [Linuxoid85](https://github.com/Linuxoid85) <linuxoid85@gmail.com> -
  разработка дистрибутива, сборка, etc.
* [AristarhBahirev](https://github.com/AristarhBahirev) - разработка Ascetico,
  дизайн сайта, работа над системой портов;
* [Sergey](https://gitlab.com/nordic_dev) - работа над системой портов, помощь
  в создании системы сборки дистрибутива.

Над логотипом работали:
* [Linuxoid85](https://github.com/Linuxoid85) - ASCII-логотип;
* [Kelptaken](https://github.com/kelptaken) - графические логотипы.
